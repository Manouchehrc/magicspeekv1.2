﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace MagicSpeek.TelegramHelper
{
    /// <summary>
    /// 
    /// </summary>
    public interface IMessageProcessor
    {
        string OnRecievedInlineQuery(TelegramTypes.X_Update xCallback);
        string OnRecievedCallbackQuery(TelegramTypes.X_Update xCallback);
        string OnRecievedVoice(TelegramTypes.X_Update xCallback);
        string OnRecievedAudio(TelegramTypes.X_Update xCallback);
        string OnRecievedVideo(TelegramTypes.X_Update xCallback);
        string OnRecievedVideoNote(TelegramTypes.X_Update xCallback);
        string OnRecievedDocument(TelegramTypes.X_Update xCallback);
        string OnRecievedPhoto(TelegramTypes.X_Update xCallback);
        string OnRecievedContact(TelegramTypes.X_Update xMessage);
        string OnRecievedLocation(TelegramTypes.X_Update xMessage);
        string OnRecievedVenue(TelegramTypes.X_Update xMessage);
        string OnRecievedMessage(TelegramTypes.X_Update xMessage);
    }

    public class resultValue
    {
        private class _Value
        {
            public bool ok { get; set; }
            public string error { get; set; }
        }
        public static string MakeJsonResult(bool _ok , string _error = "")
        {
            _Value JsonValue = new _Value 
            {
                ok = _ok,
                error = _error
            };

            return JsonConvert.SerializeObject(JsonValue);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class TelegramInterface
    {
        private String ApiKey;
        public String API_URL = "https://api.telegram.org/bot";
        public String API_FILE_URL = "https://api.telegram.org/file/bot";

        public long LastUpdateID
        {
            get
            {
                return Convert.ToInt64(HttpContext.Current.Application[ApiKey].ToString());
            }
            set
            {
                HttpContext.Current.Application.Lock();
                HttpContext.Current.Application[ApiKey] = value;
                HttpContext.Current.Application.UnLock();
            }
        }

        public TelegramInterface(String ApiKey)
        {
            this.ApiKey = ApiKey;

            if (HttpContext.Current.Application[ApiKey] == null)
                LastUpdateID = 0;
        }

        private String GetMethodUrl(String method_name)
        {
            return this.API_URL + this.ApiKey + "/" + method_name;
        }

        private TelegramTypes.X_JObjectResult CallApiMethod(String uri, dynamic Parameters_Object)
        {
            try
            {
                WebRequest req = (HttpWebRequest)HttpWebRequest.Create(uri);
                req.Method = "POST";
                req.ContentType = "application/json";
                req.Credentials = CredentialCache.DefaultCredentials;

                string json = "";
                using (var streamWriter = new StreamWriter(req.GetRequestStream()))
                {
                    json = JsonConvert.SerializeObject(Parameters_Object, Formatting.None,
                                new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Ignore
                                });
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                HttpWebResponse hwrResponse = (HttpWebResponse)req.GetResponse();
                StreamReader srResponse = new StreamReader(hwrResponse.GetResponseStream());
                return JsonConvert.DeserializeObject<TelegramTypes.X_JObjectResult>(srResponse.ReadToEnd());
            }
            catch (WebException ex)
            {
                var webResponse = ex.Response as HttpWebResponse;
                if (webResponse == null)
                    return new TelegramTypes.X_JObjectResult { ok = "ERR: " + ex.Message, result = null };

                return new TelegramTypes.X_JObjectResult { ok = "STT: " + webResponse.StatusCode.ToString(), result = null };
            }
        }

        /////////////////////////////////// All telegram method start from here ///////////////////////////////////

        /*
        "getupdates"
        "getme"
        "senddocument"
        "getUserProfilePhotos"
         */

        public JObject editMessageText(long chatid, long messageid, String msg, dynamic replymarkup = null, string parsemode = null, string inlineid = "")
        {
            TelegramTypes.Action_editMessageText param = new TelegramTypes.Action_editMessageText
            {
                chat_id = chatid,
                text = msg,
                inline_message_id = inlineid,
                message_id = messageid,
                reply_markup = replymarkup, 
                parse_mode = parsemode
            };

            String path = GetMethodUrl("editMessageText");
            TelegramTypes.X_JObjectResult feedback = CallApiMethod(path, param);
            if (feedback.ok.Contains("ERR"))
            {

            }
            else if (feedback.ok.Contains("STT"))
            {

            }
            else if (feedback.ok.ToLower() == "false")
            {

            }

            return feedback.result;
        }

        public JObject editMessageCaption(long chatid, long messageid, String caption, dynamic replymarkup = null, string inlineid = "")
        {
            TelegramTypes.Action_editMessageCaption param = new TelegramTypes.Action_editMessageCaption
            {
                chat_id = chatid,
                caption = caption,
                message_id = messageid,
                inline_message_id = inlineid,
                reply_markup = replymarkup,
            };

            String path = GetMethodUrl("editMessageCaption");
            TelegramTypes.X_JObjectResult feedback = CallApiMethod(path, param);
            if (feedback.ok.Contains("ERR"))
            {

            }
            else if (feedback.ok.Contains("STT"))
            {

            }
            else if (feedback.ok.ToLower() == "false")
            {

            }

            return feedback.result;
        }

        public JObject editMessageReplyMarkup(long chatid, long messageid, String msg, dynamic replymarkup = null, string inlineid = "")
        {
            TelegramTypes.Action_editMessageReplyMarkup param = new TelegramTypes.Action_editMessageReplyMarkup
            {
                chat_id = chatid,
                message_id = messageid,
                inline_message_id = inlineid,
                reply_markup = replymarkup,
            };

            String path = GetMethodUrl("editMessageReplyMarkup");
            TelegramTypes.X_JObjectResult feedback = CallApiMethod(path, param);
            if (feedback.ok.Contains("ERR"))
            {

            }
            else if (feedback.ok.Contains("STT"))
            {

            }
            else if (feedback.ok.ToLower() == "false")
            {

            }

            return feedback.result;
        }

        public TelegramTypes.X_Json_SendMessage SendMessage(long chatid, String msg, dynamic replymarkup = null, bool disabelnotification = false, bool disablewebpreview = false, long replytomsg = 0, string parsemode = null)
        {
            TelegramTypes.Action_SendMessage param = new TelegramTypes.Action_SendMessage
            {
                chat_id = chatid,
                text = msg,
                reply_markup = replymarkup,
                disable_notification = disabelnotification,
                reply_to_message_id = replytomsg,
                disable_web_page_preview = disablewebpreview,
                parse_mode = parsemode
            };

            String path = GetMethodUrl("sendmessage");
            TelegramTypes.X_JObjectResult feedback = CallApiMethod(path, param);
            if (feedback.ok.Contains("ERR"))
            {

            }
            else if (feedback.ok.Contains("STT"))
            {

            }
            else if (feedback.ok.ToLower() == "false")
            {

            }

            JObject jObjFeedback = feedback.result as JObject;
            if (jObjFeedback != null)
                return jObjFeedback.ToObject<TelegramTypes.X_Json_SendMessage>();
            else
            {
                return null;
            }
        }

        internal string getFilePath(string file_id)
        {
            TelegramTypes.Action_getFile getfile = new TelegramTypes.Action_getFile();
            getfile.file_id = file_id;

            TelegramTypes.X_JObjectResult feedback = CallApiMethod(GetMethodUrl("getFile"), getfile);
            JObject jObjFeedback = feedback.result as JObject;
            TelegramTypes.X_Json_File_Path x_json_file_path = jObjFeedback.ToObject<TelegramTypes.X_Json_File_Path>();

            return API_FILE_URL + this.ApiKey + "/" + x_json_file_path.file_path;
        }

        public TelegramTypes.X_Json_AfterSendVoiceMessage SendVoice(String uri, long chat_id, String caption = null, dynamic replymarkup = null)
        {
            TelegramTypes.Action_SendVoice _voice = new TelegramTypes.Action_SendVoice();
            _voice.chat_id = chat_id;
            _voice.caption = caption;
            _voice.voice = uri;
            _voice.reply_markup = replymarkup;
            
            TelegramTypes.X_JObjectResult feedback = CallApiMethod(GetMethodUrl("sendVoice"), _voice);
            JObject jObjFeedback = feedback.result as JObject;
            return jObjFeedback.ToObject<TelegramTypes.X_Json_AfterSendVoiceMessage>();
        }

        public TelegramTypes.X_Json_AfterSendVoiceMessage SendVoice(String voice_uri, long chat_id, dynamic replymarkup)
        {
            return SendVoice(voice_uri, chat_id, null, replymarkup);
        }

        public TelegramTypes.X_Json_AfterSendLocation SendLocation(float latitude, float longitude, long chat_id, dynamic replymarkup = null)
        {
            TelegramTypes.Action_SendLocation _location = new TelegramTypes.Action_SendLocation();
            _location.latitude = latitude;
            _location.longitude = longitude;
            _location.chat_id = chat_id;
            _location.reply_markup = replymarkup;

            TelegramTypes.X_JObjectResult feedback = CallApiMethod(GetMethodUrl("sendLocation"), _location);
            JObject jObjFeedback = feedback.result as JObject;
            return jObjFeedback.ToObject<TelegramTypes.X_Json_AfterSendLocation>();
        }

        public TelegramTypes.X_Json_AfterSendVenue SendVenue(float latitude, float longitude, String title, String address, long chat_id, dynamic replymarkup = null)
        {
            TelegramTypes.Action_SendVenu _venue = new TelegramTypes.Action_SendVenu();
            _venue.latitude = latitude;
            _venue.longitude = longitude;
            _venue.title = title;
            _venue.address = address;
            _venue.chat_id = chat_id;
            _venue.reply_markup = replymarkup;

            TelegramTypes.X_JObjectResult feedback = CallApiMethod(GetMethodUrl("sendVenue"), _venue);
            JObject jObjFeedback = feedback.result as JObject;
            return jObjFeedback.ToObject<TelegramTypes.X_Json_AfterSendVenue>();
        }

        public TelegramTypes.X_Json_AfterSendContact SendContact(String phone_number, String first_name, String last_name, long chat_id, dynamic replymarkup = null)
        {
            TelegramTypes.Action_SendContact _contact = new TelegramTypes.Action_SendContact();
            _contact.phone_number = phone_number;
            _contact.first_name = first_name;
            _contact.last_name = last_name;
            _contact.chat_id = chat_id;
            _contact.reply_markup = replymarkup;

            TelegramTypes.X_JObjectResult feedback = CallApiMethod(GetMethodUrl("SendContact"), _contact);
            JObject jObjFeedback = feedback.result as JObject;
            return jObjFeedback.ToObject<TelegramTypes.X_Json_AfterSendContact>();
        }

        public void SendDocument(long chat_id, String document, String caption = null, dynamic reply_markup = null)
        {
            TelegramTypes.Action_SendDocument _document = new TelegramTypes.Action_SendDocument();
            _document.chat_id = chat_id;
            _document.document = document;
            _document.caption = caption;
            _document.reply_markup = reply_markup;
            CallApiMethod(GetMethodUrl("SendDocument"), _document);
        }

        public void SendDocument(long chat_id, String document, dynamic reply_markup = null)
        {
            SendDocument(chat_id, document, null, reply_markup);
        }

        public TelegramTypes.X_Json_AfterSendPhoto SendPhoto(long chat_id, string photo, string caption = null, dynamic reply_markup = null)
        {
            TelegramTypes.Action_SendPhoto _photo = new TelegramTypes.Action_SendPhoto();
            _photo.chat_id = chat_id;
            _photo.photo = photo;
            _photo.caption = caption;
            _photo.reply_markup = reply_markup;

            TelegramTypes.X_JObjectResult feedback = CallApiMethod(GetMethodUrl("SendPhoto"), _photo);
            JObject jObjFeedback = feedback.result as JObject;
            return jObjFeedback.ToObject<TelegramTypes.X_Json_AfterSendPhoto>();
        }

        public void SendPhoto(long chat_id, string photo, dynamic reply_markup)
        {
            SendPhoto(chat_id, photo, null, reply_markup);
        }

        public TelegramTypes.X_Json_AfterSendVideo SendVideo(long chat_id, string video, long width, long height, string caption = null, dynamic reply_markup = null)
        {
            TelegramTypes.Action_SendVideo _video = new TelegramTypes.Action_SendVideo();
            _video.chat_id = chat_id;
            _video.width = width;
            _video.height = height;
            _video.video = video;
            _video.caption = caption;
            _video.reply_markup = reply_markup;


            TelegramTypes.X_JObjectResult feedback = CallApiMethod(GetMethodUrl("SendVideo"), _video);
            JObject jObjFeedback = feedback.result as JObject;
            return jObjFeedback.ToObject<TelegramTypes.X_Json_AfterSendVideo>();
        }

        //Use this method to send answers to callback queries sent from inline keyboards. The answer will be displayed to the 
        //user as a notification at the top of the chat screen or as an alert. On success, True is returned.
        public void answerCallbackQuery()
        {
            
        }

        internal void TestMarkup(long _message_id)
        {
            //TelegramTypes.X_InlineKeyboardMarkup inlineKeyboardMarkup = new TelegramTypes.X_InlineKeyboardMarkup();

            //inlineKeyboardMarkup.InsertButton(new TelegramTypes.X_InlineKeyboardButton
            //{
            //    text = "Test Button 1",
            //    url = "www.google.com"
            //});
            //inlineKeyboardMarkup.EndRow();

            //inlineKeyboardMarkup.InsertButton(new TelegramTypes.X_InlineKeyboardButton
            //{
            //    text = "Test Button 2",
            //    url = "www.google.com"
            //});
            //inlineKeyboardMarkup.EndRow();

            //TelegramTypes.Action_SendMessage sendMsg = new TelegramTypes.Action_SendMessage();
            //sendMsg.chat_id = _message_id;
            //sendMsg.text = "Test Messages";
            //sendMsg.reply_markup = inlineKeyboardMarkup;

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            //TelegramTypes.X_ReplyKeyboardMarkup replay_markup = new TelegramTypes.X_ReplyKeyboardMarkup();

            //replay_markup.InsertButton(new TelegramTypes.X_KeyboardButton
            //    {
            //        text = "First Button OK"
            //    });
            //replay_markup.EndRow();

            //TelegramTypes.Action_SendMessage sendMsg = new TelegramTypes.Action_SendMessage();
            //sendMsg.chat_id = _message_id;
            //sendMsg.text = "Test Messages";
            //sendMsg.reply_markup = replay_markup;

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            //TelegramTypes.X_ReplyKeyboardRemove remove_keyboard = new TelegramTypes.X_ReplyKeyboardRemove();

            //TelegramTypes.Action_SendMessage sendMsg = new TelegramTypes.Action_SendMessage();
            //sendMsg.chat_id = _message_id;
            //sendMsg.text = "Test Messages";
            //sendMsg.reply_markup = remove_keyboard;

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            //CallApiMethod(GetMethodUrl("sendmessage"), sendMsg);
        }

        private int _inlineQuery_index = 0;

        public void TestInlineQuery(string inline_id)
        {
            TelegramTypes.Action_AnswerInlineQuery ansInlineQ = new TelegramTypes.Action_AnswerInlineQuery
            {
                inline_query_id = inline_id
            };
            TelegramTypes.X_InlineQueryResultArticle article = new TelegramTypes.X_InlineQueryResultArticle();
            article.id = _inlineQuery_index.ToString();
            article.title = "List 1";
            article.input_message_content = new TelegramTypes.X_InputTextMessageContent
            {
                message_text = "This is msg text"
            };
            ansInlineQ.AddQueryResult(article);

            CallApiMethod(GetMethodUrl("answerInlineQuery"), ansInlineQ);
            _inlineQuery_index++;
        }


        public string ParseUpdate(Stream stream, IMessageProcessor theBotController)
        {
            stream.Seek(0, SeekOrigin.Begin);
            string json = new StreamReader(stream).ReadToEnd();

            if (string.IsNullOrEmpty(json))
            {
                return resultValue.MakeJsonResult( false);
            }

            // DESERIALIZE THE UPDATE
            TelegramTypes.X_Update theUpdate = JsonConvert.DeserializeObject<TelegramTypes.X_Update>(json);

            if (theUpdate == null)
            {
                return resultValue.MakeJsonResult( false);
            }

            if (LastUpdateID >= theUpdate.update_id)
            {
                // the update has already been processed
                return resultValue.MakeJsonResult(false);
            }

            // Save last update
            // This ID becomes especially handy if you’re using Webhooks, 
            // since it allows you to ignore repeated updates or to restore 
            // the correct update sequence, should they get out of order.
            LastUpdateID = theUpdate.update_id;

            if (theUpdate.inline_query != null)
            { // call inline query processor

                return theBotController.OnRecievedInlineQuery(theUpdate);
            }
            else if (theUpdate.callback_query != null)
            { // call query processor
                return theBotController.OnRecievedCallbackQuery(theUpdate);
            }
            else if (theUpdate.message != null)
            {
                if ( String.IsNullOrEmpty(theUpdate.message.text))
                { // it's a document, photo or vide, with 0-200 character caption 
                    if (theUpdate.message.audio != null)
                    {
                        return theBotController.OnRecievedAudio(theUpdate);
                    }
                    else if (theUpdate.message.video != null)
                    {
                        return theBotController.OnRecievedVideo(theUpdate);
                    }
                    else if (theUpdate.message.voice != null)
                    {
                        return theBotController.OnRecievedVoice(theUpdate);
                    }
                    else if (theUpdate.message.venue != null)
                    {
                        return theBotController.OnRecievedVenue(theUpdate);
                    }
                    else if (theUpdate.message.contact != null)
                    {
                        return theBotController.OnRecievedContact(theUpdate);
                    }
                    else if (theUpdate.message.photo != null)
                    {
                        return theBotController.OnRecievedPhoto(theUpdate);
                    }
                }
                else
                { // it's a text message
                    return theBotController.OnRecievedMessage(theUpdate);
                }
            }

            return resultValue.MakeJsonResult(false);
        }

    }
}